package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class MealsServiceTest {
	MealsService mealService = new MealsService();
	
	//Regular
	
	 @Test
		public void testDrinksRegular() {
			List<String> test = mealService.getAvailableMealTypes(MealType.DRINKS);
			int length = test.size();
			assertTrue(length!=0);
		}
		
	 //Exception
		@Test
		public void testDrinksException() {
		List<String> test = mealService.getAvailableMealTypes(null);
			assertTrue(test.get(0).equals("No Brand Available"));
		}
		
		//BoundaryIn
		@Test
		public void testDrinksBoundaryIn() {
			List<String> test = mealService.getAvailableMealTypes(MealType.DRINKS);
			int length = test.size();
			assertTrue(length>3);
		}

		//BoundaryOut
		@Test
		public void testDrinksBoundaryOut() {
			List<String> test = mealService.getAvailableMealTypes(null);
			int length = test.size();
			assertTrue(length<=1);
		}
		

	}
